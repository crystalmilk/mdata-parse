import os
import socket
import database.ins

# open how many website instance on this machine
PROCESS_PER_WEBSITE=1

# in debug mode, how many url to test fro one website
DEBUG_HOW_MANY_URL=50

# time assigned to each subproces, unit: hour
PERIOD = 8

# number of detail url feched from pserver each time
QUANTITY_URL=100

# browser reopen limit
BROWSER_OPEN_LIMIT=10

# name of the computer, used to construct table name and fetch deploy website from pserver
HANDLER = socket.gethostname()

# total websites pool
CHAMPION_DIR_NAME='pool'

# path used in program
PATH_LOG = os.path.join(os.path.dirname(__file__),'log')
PATH_BLOCK = os.path.join(os.path.dirname(__file__),'block')
PATH_KEYWORD = os.path.join(os.path.dirname(__file__),'keyword')
PATH_URL = os.path.join(os.path.dirname(__file__),'url')
PATH_PROXY = os.path.join(os.path.dirname(__file__),'proxy.txt')

# max amount of subprocess to run
MAX_AMOUNT_PROCESS = 2000

# local databse connection info
LOCAL_DB_INFO = {
    'user':'root',
    'password':'',
    'host':'localhost',
    'database':'spider'
}

# local database file path
DB_TABLE_LOCATION ="/var/lib/mysql/"

# warehouse db info, MUST BE WITHIN THE SAME LAN WITH SPIDER 9
WAREHOUSE_DB_INFO = {
    'user':'root',
    'password':'',
    'host':'192.168.1.86',
    'database':'mdata_warehouse'
}

# Pserver database connection info
PSERVER_DB_INFO = {
    'user':'wenchao.zheng',
    'password':'mdata2016_8',
    'host':'47.88.87.178',
    'database':'mdata_api'
}

# table name constant
CREATE_TABLE_SQL_TABLE = "table_create_sqls"
HTML_TABLES_PARSE_LOG_TABLE = "html_tables_parse_log"

# PServer database upload info
PSERVER_UPLOAD_INFO = {
    'user':'wenchao.zheng',
    'password':'mdata2016_8',
    'host':'47.88.87.178',
    'database':'malama_ca_mdata'
}
# TServer database upload info
TSERVER_UPLOAD_INFO = {
    'user':'wenchao.zheng',
    'password':'mdata2016_8',
    'host':'120.76.73.21',
    'database':'maibeimei_mdata'
}

QUERY_TABLE_CREATE_SQL = ''
