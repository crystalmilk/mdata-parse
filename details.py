import sys,os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),os.pardir))
import pprint
import uuid
import time
import urllib.parse
import utility
import config
try:
    import parse
except:
    from . import parse
try:
    import downloader
except:
    from . import downloader

class details:
    # initialize logger in constructor
    def __init__(self, debug=False, logger=None):
        self.debug = debug
        if logger is None:
            logger = utility.init_logger('detail_test')
        self.logger = logger
        self.downloader = downloader.downloader(self.debug, self.logger)

    overall = {
        'usa': '0',
        'bad': '0',
        'detail_only': '0',
        'comment': 'so far so good, or author forgot to leave comments',
        'author': 'computer',
    }
    site_info = {
        'id': '22',
        'name': 'Amazon (US)',
        'url': 'http://www.amazon.com'
    }
    detail_xpath = {
        # 'render': '0',
        'seller': [
            '//*[contains(text(),"old by")]/a[1]'
        ],
        'title': [
            '//span[contains(@id,"productTitle")]'
        ],
        'price': [
            '//span[contains(@id,"priceblock")]'
        ],
        'brand': [
            '//*[@id="brand"]'
        ],
        'variation': [
            ''
        ],
        'main_image': [
            '//div[contains(@id,"imgTagWrapperId")]/img/@src'
        ],
        'images': [
            '//div[contains(@id, "altImage")]/ul[contains(@class, "button-list")]'
        ],
        'sell_point': [
            '//div[@id="feature-bullets"]'
        ],
        'description': [
            '//div[@id="feature-bullets"]'
        ],
        'star': [
            '(//i[contains(@class,"a-icon-star")]/span)[1]'
        ],
        'review': [
            '//div[contains(@id,"ratings-sample")]'
        ],
        'mrsp': [
            '//span[@class="regular-price"]'
        ],
        'model': [
            '//span[@itemprop="model"]'
        ],
        'condition': [
            ''
        ],
        'upc': [
            '//div[span/text()="UPC"]/following-sibling::div'
        ],
        'status': [
            '//div[@class="availability-postcard"]'
        ],
        'shipping_cost': [
            ''
        ],
        'shipping_cost_desc': [
            ''
        ],
        'shipping_time': [
            ''
        ],
        'shipping_time_desc': [
            ''
        ],
        'service_desc': [
            ''
        ],
        'question': [
            ''
        ]
    }

    meta_xpath = {
        'meta_title': [
            '//meta[@property="og:title"]/@content',
            '//meta[@name="Title"]/@content',
            '//meta[@name="title"]/@content',
            '//meta[@itemprop="name"]/@content'
        ],
        'meta_brand': [
            '//meta[@itemprop="brand"]/@content'
        ],
        'meta_url': [
            '//meta[@property="og:url"]/@content'
        ],
        'meta_img': [
            '//meta[@property="og:image"]/@content'
        ],
        'meta_price': [
            '//meta[@property="og:price"]/@content',
            '//meta[contains(@property, "price")]/@content',
            '//meta[@itemprop="price"]/@content'
        ],
        'meta_description': [
            '//meta[@property="og:description"]/@content',
            '//meta[@name="Description"]/@content',
            '//meta[@name="description"]/@content',
            '//meta[@itemprop="description"]/@content'
        ],
        'meta_sellpoints': [
            '//meta[contains(@name, "meta")]/@content'
        ],
        'meta_condition': [
            '//meta[contains(@property, "og:availability")]/@content'
        ],
        'meta_status': [
            '//meta[contains(@itemprop, "availability")]/@content'
        ],
        'meta_stars': [
            '//meta[@itemprop="ratingValue"]/@content'
        ]
    }

    def get_html(self, url):
        try:
            return self.downloader.get_html(url, self.detail_xpath['render'])
        except Exception as e:
            self.logger.exception(e)

    def get_title(self, node):
        try:
            # set undefined default value
            content = ''
            # use every xpath provided to retrieve content
            for xpath in self.detail_xpath['title']:
                content = parse.text_deep(node, xpath, '', '')
                # if content found, break loop
                if len(content) > 0:
                    break
            # if nothing found using detail xpath, try meta xpath
            if len(content) == 0:
                content = self.meta_get_content(node, 'meta_title')
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_price(self, node):
        try:
            # undefined
            content = -1
            # use every xpath provided to retrieve content
            for xpath in self.detail_xpath['price']:
                content = parse.text_deep(node, xpath, '', '')
                # if content found, break loop
                if len(content) > 0:
                    break
            # if got nothing from detail xpath, try meta xpath
            if len(content) == 0:
                content = self.meta_get_content(node, 'meta_price')
            price = parse.price_number(content)
            return price
        except Exception as e:
            self.logger.exception(e)

    def get_seller(self, node):
        try:
            # undefined
            content = ''
            for xpath in self.detail_xpath['seller']:
                content = parse.text(node, xpath, '')
                if len(content) > 0:
                    break
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_variation(self, node):
        try:
            # undefined
            content = ''
            for xpath in self.detail_xpath['variation']:
                content = parse.inner_html(node, xpath, '')
                if len(content) > 0:
                    break
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_mrsp(self, node):
        try:
            # undefined
            content = ''
            for xpath in self.detail_xpath['mrsp']:
                content = parse.text(node, xpath, '')
                if len(content) > 0:
                    break
            price = parse.price_number(content)
            return price
        except Exception as e:
            self.logger.exception(e)
            return -1

    def get_brand(self, node):
        try:
            # undefined
            content = ''
            for xpath in self.detail_xpath['brand']:
                content = parse.text(node, xpath, '')
                if len(content) > 0:
                    return content
            if len(content) == 0:
                content = self.meta_get_content(node, 'meta_brand')
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_model(self, node):
        try:
            # undefined
            content = ''
            for xpath in self.detail_xpath['model']:
                content = parse.text(node, xpath, '')
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_condition(self, node):
        try:
            # undefined
            content = ''
            for xpath in self.detail_xpath['condition']:
                content = parse.text(node, xpath, '')
                if len(content) > 0:
                    break
            if "refurbish" in content:
                return 4
            elif "used" in content:
                return 2
            elif "open" in content:
                return 3
            else:
                return 1
        except Exception as e:
            self.logger.exception(e)

    def get_main_image(self, node):
        try:
            content = parse.attribute_smart(node, self.detail_config['main_image'], '', '')
            if len(content) == 0:
                content = self.meta_get_content(node, 'meta_img')
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_images(self, node):
        try:
            images = parse.attribute_deep(node, self.detail_config['images'], 'img/@src', '', '|||||')
            return images
        except Exception as e:
            self.logger.exception(e)

    def get_sellpoint(self, node):
        try:
            content = parse.text_deep(node, self.detail_config['sell_point'], '', ' ')
            if len(content) == 0:
                content = self.meta_get_content(node, 'meta_sellpoints')
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_upc(self, node):
        try:
            return parse.text(node, self.detail_config['upc'], '')
        except Exception as e:
            self.logger.exception(e)

    def get_status(self, node):
        try:
            content = parse.text(node, self.detail_config['status'], '')
            if len(content) == 0:
                content = self.meta_get_content(node, 'meta_status')
            availability = 1  # default 1: in stock
            if "out" in content:
                availability = 0  # out of stock if content retrieved contains 'out'(as in 'out of stock')
            return availability
        except Exception as e:
            self.logger.exception(e)

    def get_star(self, node):
        try:
            star_text = parse.text_deep(node, self.detail_config['star'], '', ' ')
            if len(star_text) == 0:
                star_text = self.meta_get_content(node, 'meta_stars')
            star_number = parse.price_number(star_text)
            return star_number
        except Exception as e:
            self.logger.exception(e)

    def get_shipping_cost(self, node):
        try:
            content = parse.text(node, self.detail_config['shipping_cost'], '')
            content = 0
            # extract the cost price!
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_shipping_cost_desc(self, node):
        try:
            return parse.text(node, self.detail_config['shipping_cost_desc'], '')  # could be innerhtml
        except Exception as e:
            self.logger.exception(e)

    def get_shipping_time(self, node):
        try:
            content = parse.text(node, self.detail_config['shipping_time'], '')
            content = 0
            return content
        except Exception as e:
            self.logger.exception(e)

    def get_shipping_time_desc(self, node):
        try:
            return parse.text(node, self.detail_config['shipping_time_desc'], '')
        except Exception as e:
            self.logger.exception(e)

    def get_description(self, node):
        try:
            return parse.inner_html(node, self.detail_config['description'], '')
        except Exception as e:
            self.logger.exception(e)

    def get_meta_description(self, node):
        return self.meta_get_content(node, 'meta_description')

    def get_service_desc(self, node):
        try:
            return parse.text(node, self.detail_config['service_desc'], '')
        except Exception as e:
            self.logger.exception(e)

    def get_question(self, node):
        try:
            return parse.inner_html(node, self.detail_config['question'], '')
        except Exception as e:
            self.logger.exception(e)

    def get_review(self, node):
        try:
            return parse.inner_html(node, self.config['review'], '')
        except Exception as e:
            self.logger.exception(e)

    # def detail_get_content(self, node, xpathKey):
    #     try:
    #         for path in self.detail_xpath[xpathKey]:
    #             content = parse.text(node, path, 'content')
    #             if len(content) > 0:
    #                 return content
    #     except Exception as e:
    #         self.logger.exception(e)

    def meta_get_content(self, node, xpathKey):
        try:
            for path in self.meta_xpath[xpathKey]:
                content = parse.attribute_smart(node, path, 'content', '')
                if len(content) > 0:
                    return content
            return ''
        except Exception as e:
            self.logger.exception(e)

    def detail_product_filter(self, product):
        try:
            product = product
            status = 0  # default status value

            # out of stock => status = -1 : is dead
            if product["good_status"] == 0:
                product["good_status"] = -1
                product["detail_status_reason"] = "out of stock"
                return product
            # other dead reasons
            dead_reason = ""
            if len(product["good_title"]) == 0 or product["good_title"] is None:
                status = -1
                dead_reason += "no title"
            if (len(product["good_brand"]) == 0 or product["good_brand"] is None) and len(
                    self.detail_config["brand"]) > 0:
                status = -1
                if len(dead_reason) > 0:
                    dead_reason += ", "
                dead_reason += "no brand"
            if float(product["good_price"]) < 1 or product["good_price"] is None:
                status = -1
                if len(dead_reason) > 0:
                    dead_reason += ", "
                dead_reason += "abnormal price"
            if (len(product["good_main_image"]) == 0 or product["good_main_image"] is None) and len(
                    self.detail_config["main_image"]) > 0:
                status = -1
                if len(dead_reason) > 0:
                    dead_reason += ", "
                dead_reason += "no main image"
            if status == -1:
                product["good_status"] = status
                product["detail_status_reason"] = dead_reason
                return product

            # warning
            warning_reason = ""
            if len(product["goods_other_images"]) == 0 and len(self.detail_config["images"]) > 0:
                status = 1
                warning_reason = "no images"
            if len(product["good_detail_variation"]) == 0 and len(self.detail_config["variation"]) > 0:
                status = 1
                if len(warning_reason) > 0:
                    warning_reason += ", "
                warning_reason += "no variation"
            if status == 1:
                product["status"] = status
                product["detail_status_reason"] = warning_reason
                return product
        except Exception as e:
            self.logger.exception(e)
        # no warning or error found => status = 2 : normal
        product["good_status"] = 2
        product["detail_status_reason"] = "normal"
        return product

    def detail_construct_tree(self, htmlPage):
        try:
            return parse.construct_tree(htmlPage)
        except Exception as e:
            self.logger.exception(e)

    def detail_adjust_content_for_readability(self, product, key):
        if key not in product.keys():
            return ''
        if len(product[key]) < 200:
            return product[key]
        return (product[key][:200] + ' ,rest content is hidden for readability')
    
    def detail(self, product):
        url = ''
        goods_id1 = ''
        goods_id2 = ''
        website_name = ''
        website_currency = ''

        if 'good_spider_url' in product:
            url = product["good_spider_url"]
        if 'mdata_goods_id1' in product:
            goods_id1 = product["mdata_goods_id1"]
        if 'mdata_goods_id2' in product:
            goods_id2 = product["mdata_goods_id2"]
        if 'good_website_name' in product:
            website_name = product['good_website_name']
        if 'good_website_currency' in product:
            website_currency = product['good_website_currency']

        self.logger.info('mdata_goods_id1:' + str(goods_id1))
        self.logger.info('mdata_goods_id2:' + str(goods_id2))
        self.logger.info('good_website_name:' + str(website_name))

        # get detail page
        html_page, status, spent, method, block = self.get_html(url)
        self.html = html_page
        # self.record_http_status(self.current_ip,len(html_page),time_cost,method,"Hey")

        if status != 'success' or block:
            return (False, {})

        # construct tree, return root node
        node = self.construct_tree(html_page)

        # using root node and dic to parse each attribute
        # these functions can be overwritten by inherited classes
        title = self.get_title_by_xpath(node)
        price = self.get_price_by_xpath(node)
        seller = self.get_seller_by_xpath(node)
        variation = self.get_variation_by_xpath(node)
        mrsp = self.get_mrsp_by_xpath(node)
        brand = self.get_brand_by_xpath(node)
        model = self.get_model_by_xpath(node)
        condition = self.get_condition_by_xpath(node)
        main_image = self.get_main_image_by_xpath(node)
        images = self.get_images_by_xpath(node)
        sell_point = self.get_sellpoint_by_xpath(node)
        upc = self.get_upc_by_xpath(node)
        status = self.get_status_by_xpath(node)
        star = self.get_star_by_xpath(node)
        shipping_cost = self.get_shipping_cost_by_xpath(node)
        shipping_cost_desc = self.get_shipping_cost_desc_by_xpath(node)
        shipping_time = self.get_shipping_time_by_xpath(node)
        shipping_time_desc = self.get_shipping_time_desc_by_xpath(node)
        description = self.get_description_by_xpath(node)
        meta_description = self.get_meta_description_by_xpath(node)
        service_desc = self.get_service_desc_by_xpath(node)
        question = self.get_question_by_xpath(node)
        review = self.get_review_by_xpath(node)

        # define a dictionary to hold attributes
        product_detail = {}

        current_time = str(int(time.time()))

        # assign attributes to dictionary (key,value) pairs
        product_detail["good_fetch_id"] = str(uuid.uuid1())
        product_detail["mdata_goods_id1"] = goods_id1
        product_detail["mdata_goods_id2"] = goods_id2
        product_detail["good_title"] = str(title)
        product_detail["good_price"] = str(price)
        product_detail["good_spider_url"] = url
        product_detail["good_website_name"] = website_name
        product_detail["good_website_currency"] = website_currency
        product_detail["good_seller"] = str(seller)
        product_detail["good_variation"] = str(variation)
        product_detail["good_initial_time"] = current_time
        product_detail["good_mrsp_price"] = mrsp
        product_detail["good_brand"] = str(brand)
        product_detail["good_model"] = model
        product_detail["good_condition"] = condition
        product_detail["good_main_image"] = main_image
        product_detail["goods_other_images"] = images
        product_detail["good_sell_point"] = str(sell_point)
        product_detail["good_upc"] = upc
        product_detail["good_status"] = status
        product_detail["good_rating_stars"] = str(star)
        product_detail["good_shipping_cost"] = shipping_cost
        product_detail["good_shipping_cost_desc"] = shipping_cost_desc
        product_detail["good_shipping_time"] = shipping_time
        product_detail["good_shipping_time_desc"] = shipping_time_desc
        product_detail["good_description"] = str(description)
        product_detail["good_meta_description"] = str(meta_description)
        product_detail["good_service_desc"] = service_desc
        product_detail["good_question"] = question
        product_detail["good_html"] = str(html_page)
        product_detail["good_reviews"] = str(review)
        product_detail["good_history_update_handler"] = config.HANDLER

        product_detail = self.detail_product_filter(product_detail)

        if product_detail["good_status"] == 1 or product_detail["good_status"] == 2:
            self.logger.info("This product is alive")
        elif product_detail["good_status"] == -1:
            self.logger.info("This product is dead")
        else:
            self.logger.warning(product_detail["good_status"] + ": " + product_detail["detail_status_reason"])

        if self.debug:
            product_detail['good_html'] = self.detail_adjust_content_for_readability(product_detail, 'good_html')
            product_detail['good_reviews'] = self.detail_adjust_content_for_readability(product_detail, 'good_reviews')
            product_detail['good_description'] = self.detail_adjust_content_for_readability(product_detail,
                                                                                            'good_description')
            product_detail['good_detail_variation'] = self.detail_adjust_content_for_readability(product_detail,
                                                                                                 'good_detail_variation')
            product_detail['good_sell_point'] = self.detail_adjust_content_for_readability(product_detail,
                                                                                           'good_sell_point')

        return (True, product_detail)

    def test(self, detail_url, printing=True):
        pp = pprint.PrettyPrinter(indent=4)
        product = {"good_spider_url":detail_url,
                         "mdata_goods_id1":'abcd',
                         "mdata_goods_id2": 'efgh',
                         "good_website_name": self.site_info['name']}
        product_detail=self.detail(product)
        if printing:
            pp.pprint(product_detail)

if __name__ == "__main__":
    champion=Details(debug=True)
    champion.test('laptop')
    champion.clean()