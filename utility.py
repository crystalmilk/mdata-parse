import os
import rune
import pymysql
import logging

def init_logger(logger_name='default'):

    # formatter = logging.Formatter('[%(asctime)s] - %(levelname)s -  %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    formatter = logging.Formatter('[%(asctime)s] - %(levelname)s -  %(message)s')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)

    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(console_handler)

    return logger

def create_directory(logger=None):
    if logger is None:
        logger = init_logger('create_directory')
    if not os.path.exists(rune.PATH_LOG):
        os.makedirs(rune.PATH_LOG)
        logger.info('{0} just created'.format(rune.PATH_LOG))

    if not os.path.exists(rune.PATH_KEYWORD):
        os.makedirs(rune.PATH_KEYWORD)
        logger.info('{0} just created'.format(rune.PATH_KEYWORD))

    if not os.path.exists(rune.PATH_URL):
        os.makedirs(rune.PATH_URL)
        logger.info('{0} just created'.format(rune.PATH_URL))

    if not os.path.exists(rune.PATH_BLOCK):
        os.makedirs(rune.PATH_BLOCK)
        logger.info('{0} just created'.format(rune.PATH_BLOCK))

    # remove_champion_log=input('remove champion log?')

def download_proxy(logger=None):
    if logger is None:
        logger = init_logger('download_proxy')
    cnx= pymysql.connect(user='spider_upload',password='spider2016',
                          host='47.88.87.178',
                          database='mdata_api',
                          cursorclass=pymysql.cursors.DictCursor)
    cursor= cnx.cursor()
    sql= 'select ip from mdata_api.mdata_proxy_ip'
    cursor.execute(sql)
    list_data= cursor.fetchall()
    logger.info('found proxy number:'+str(len(list_data)))
    return_list= []

    for item in list_data:
        return_list.append(item['ip'])

    logger.info('received proxy number:'+str(len(return_list)))
    with open('proxy.txt','w') as f:
        content = '\n'.join(return_list)
        f.write(content)

    logger.info('proxy.txt is updated')

def filter_proxy(ip):
    return True

def show_ip_availability():
    pass

logger = init_logger()

if __name__=="__main__":
    create_directory()
    download_proxy()
