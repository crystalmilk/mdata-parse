import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),os.pardir))
import pymysql
import config
import utility

class ins():
    def __init__(self,logger=None):
        info = config.LOCAL_DB_INFO
        self.local_connection = pymysql.connect(user=info["user"], password=info["password"],host=info["host"],database = info["database"],charset='utf8')
        self.local_dic_cursor=self.local_connection.cursor(pymysql.cursors.DictCursor)
        self.local_tuple_cursor=self.local_connection.cursor(pymysql.cursors.Cursor)
        self.pserver_connection=None
        self.pserver_cursor=None
        self.warehouse_connection=None
        self.warehouse_cursor=None
        self.logger=logger
        if logger is None:
            self.logger = utility.init_logger('database')
        self.logger.info('database instance initiated')

    def get_pserver_api_cursor(self,cursor_type):
        try:
            info = config.PSERVER_DB_INFO
            self.pserver_connection = pymysql.connect(autocommit=True,user=info["user"], password=info["password"],host=info["host"],database = info["database"], charset='utf8')
            self.pserver_cursor=self.pserver_connection.cursor(cursor_type)
            return self.pserver_cursor
        except Exception as e:
            self.logger.error(str(e))

    def get_pserver_spider_cursor(self,cursor_type):
        try:
            info = config.PSERVER_DB_INFO
            self.pserver_connection = pymysql.connect(autocommit=True,user=info["user"], password=info["password"],host=info["host"],database = 'mdata_spider', charset='utf8')
            self.pserver_cursor=self.pserver_connection.cursor(cursor_type)
            return self.pserver_cursor
        except Exception as e:
            self.logger.error(str(e))

    # spider 9 cursor
    def get_warehouse_cursor(self, cursor_type):
        try:
            info = config.WAREHOUSE_DB_INFO
            self.warehouse_connection = pymysql.connect(autocommit=True, user=info["user"], password=info["password"],
                                                      host=info["host"], database=info["database"], charset='utf8')
            self.warehouse_cursor = self.warehouse_connection.cursor(cursor_type)
            return self.warehouse_cursor
        except Exception as e:
            self.logger.error(str(e))

    def close_pserver_connection(self):
        try:
            if self.pserver_cursor is not None:
                self.pserver_cursor.close()
                self.pserver_cursor=None
            if self.pserver_connection is not None:
                self.pserver_connection.close()
                self.pserver_connection=None
        except:
            self.logger.warning('close pserver connection occurs exception')

    def execute_query_without_return(self,cursor, query, data=""):
        try:
            if not isinstance(data,str) and len(data)>0:
                data = list(data)
                cursor.execute(query,(data))
            else:
                cursor.execute(query)
            # close pserver conection if connected
            self.close_pserver_connection()
        except Exception as e:
            self.logger.error(str(e))
            self.logger.error(query)

    def execute_query_with_return(self,cursor, query):
        try:
            cursor.execute(query)
            row_list = cursor.fetchall()
            # close pserver connection if connected
            self.close_pserver_connection()
            return row_list
        except Exception as e:
            self.logger.error(str(e))
            self.logger.error(query)

def main():
   ins()

if __name__=='__main__':
    main()
