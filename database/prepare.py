import datetime
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),os.pardir))
import config
import pymysql
try:
    import instance
except:
    pass

def sync_table(instance,table_name):
    try:
        create_table(instance,table_name)
        select_sql = "SELECT * FROM {0};".format(table_name)
        insert_sql_template = "INSERT INTO {0} VALUES {1};"
        pserver_tuple_cursor=instance.get_pserver_api_cursor(pymysql.cursors.Cursor)
        insertData = instance.execute_query_with_return(pserver_tuple_cursor,select_sql)
        preparedData = []
        for data in insertData:
            t = ()
            for d in data:
                if isinstance(d,datetime.datetime):
                    t += (d.strftime('%Y-%m-%d %H:%M:%S'),)
                elif isinstance(d,type(None)):
                    t += ("",)
                else:
                    t += (d,)
            preparedData.append(t)
        insert_sql = insert_sql_template.format(table_name,','.join(map(str,preparedData)))
        instance.execute_query_without_return(instance.local_tuple_cursor,insert_sql)
    except Exception as e:
        instance.logger.exception(str(e))
        try:
            instance.logger.error(select_sql)
            instance.logger.error(insert_sql_template)
            instance.logger.error(insert_sql)
        except:
            pass

def create_table(instance,old_table_name,new_table_name="",drop_prv=True):
    try:
        fetch_sql = "SELECT create_sql from {0} where create_table_name='{1}';".format(config.CREATE_TABLE_SQL_TABLE,old_table_name)
        table_drop_sql = "DROP TABLE IF EXISTS {0};".format(old_table_name)
        pserver_tuple_cursor=instance.get_pserver_api_cursor(pymysql.cursors.Cursor)
        table_create_sql= instance.execute_query_with_return(pserver_tuple_cursor,fetch_sql)[0][0]
        if new_table_name:
            table_create_sql = table_create_sql.replace(old_table_name,new_table_name)
        if drop_prv:
            instance.execute_query_without_return(instance.local_tuple_cursor,table_drop_sql)
        else:
            split_sql = table_create_sql.split("`",1)
            table_create_sql = split_sql[0] + "IF NOT EXISTS " + "`" + split_sql[1]
        instance.execute_query_without_return(instance.local_tuple_cursor, table_create_sql)

    except Exception as e:
        instance.logger.exception(str(e))
        try:
            instance.logger.error(fetch_sql)
            instance.logger.error(table_drop_sql)
            instance.logger.error(table_create_sql)
        except:
            pass

def create_detail_table(instance):
    detail_table_name = "mdata_" + config.GOODS_DETAIL_TABLE + "_" + config.HANDLER+ "_"+datetime.datetime.now().strftime("%Y%m%d%H")
    create_table(instance, config.GOODS_DETAIL_TABLE, detail_table_name, False)
    return detail_table_name

def sync_needed_tables(instance):
    # sync table 'spider_keywords'
    instance.logger.info("[" + datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S.%f") + "]"+ "Synchronizing Table " + config.KEYWORD_TABLE)
    sync_table(instance,config.KEYWORD_TABLE)

    # sync table 'spider_websites'
    instance.logger.info("[" + datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S.%f") + "]"+ "Synchronizing Table " + config.WEBSITE_TABLE)
    sync_table(instance,config.WEBSITE_TABLE)

    # sync table 'mdata_application'
    instance.logger.info("[" + datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S.%f") + "]"+ "Synchronizing Table " + config.MDATA_APPLICATION_TABLE)
    sync_table(instance,config.MDATA_APPLICATION_TABLE)

    # sync table 'spider_brand'
    instance.logger.info("[" + datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S.%f") + "]"+ "Synchronizing Table " + config.BRAND_TABLE)
    sync_table(instance,config.BRAND_TABLE)

def create_needed_tables(instance):

    # instance.logger.info("[" + datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S.%f") + "]"+ "Creating Table " + config.GOODS_PROCESS_TABLE)
    # create_table(instance,config.GOODS_PROCESS_TABLE,drop_prv=False)

    # instance.logger.info("[" + datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S.%f") + "]"+ "Creating Table " + config.GOODS_HISTORY_PRICE_TABLE)
    # create_table(instance,config.GOODS_HISTORY_PRICE_TABLE,drop_prv=False)

    instance.logger.info("[" + datetime.datetime.now().strftime(
        "%d/%m/%y %H:%M:%S.%f") + "]" + "Creating Table " + config.GOODS_DETAIL_HTML_TABLE)
    create_table(instance,config.GOODS_DETAIL_HTML_TABLE, drop_prv=False)

def fetch_proxy_list(instance):
    try:
        pserver_dic_cursor=instance.get_pserver_api_cursor(pymysql.cursors.DictCursor)
        sql= 'select ip from mdata_api.mdata_proxy_ip'
        pserver_dic_cursor.execute(sql)
        list_data= pserver_dic_cursor.fetchall()
        return_list= []
        for item in list_data:
            return_list.append(item['ip'])
        return return_list
    except Exception as e:
        instance.logger.error(sql)
        instance.logger.exception(str(e))

def fetch_website_list(instance):
    try:
        fetch_sql = "select website_name,website_category,website_currency from {0};".format(config.WEBSITE_TABLE,config.HANDLER)
        website_list = instance.execute_query_with_return(instance.local_dic_cursor,fetch_sql)
        return website_list
    except Exception as e:
        instance.logger.exception(str(e))

def fetch_deploy_website_list(instance):
    try:
        fetch_sql='select website from {0} where is_ready=1 and (select_all=1 or `{1}`=1);'.format(config.WEBSITE_DEPLOY_STATUS,config.HANDLER)
        instance.logger.info('fetch website sql:'+fetch_sql)
        cursor=instance.get_pserver_spider_cursor(pymysql.cursors.Cursor)
        website_list=instance.execute_query_with_return(cursor,fetch_sql)
        website_list=[website[0] for website in website_list]
        return website_list
    except Exception as e:
        print(str(e))
        print(fetch_sql)
        return []

# fetch html_tables needed for parse from html log table
def fetch_html_table_list(instance):
    # assign my serial to the log record
    update_parse_handler(instance)
    try:

        query = "SELECT table_name FROM {0} WHERE parse_handler = {1}".format(config.HTML_TABLES_PARSE_LOG_TABLE, config.HANDLER)
        htmlTableList = instance.execute_query_with_return(instance.local_dic_cursor, query)
        return htmlTableList
    except Exception as e:
        instance.logger.exception(str(e))

def update_parse_handler(instance):
    query = "UPDATE {0} SET parse_handler = {1} WHERE "

# fetch html_

def test_detail_table_name():
    detail_table_name = "mdata_" + config.GOODS_DETAIL_TABLE + "_" + config.HANDLER+ "_"+datetime.datetime.now().strftime("%Y%m%d%H")
    print(detail_table_name)
