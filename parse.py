from lxml import html
from lxml import etree
from string import Template
import logging
import re

def prepare_logger():
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)

    logger = logging.getLogger('parse')
    logger.setLevel(logging.DEBUG)
    # logger.addHandler(console_handler)

    return logger

parse_logger = prepare_logger()

# check xpath emptyness and syntax, simple version
def check_xpath(path):
    try:
        parse_logger.info("check xpath:" + path)
        if path is None:
            parse_logger.info('xpath is none')
        if not path.strip():
            parse_logger.info('xpath strip to none')
            return False
        result = path.startswith('.') or path.startswith('//') or path.startswith('(')
        if not result:
            parse_logger.warn('xpath syntax error')
            return False
        parse_logger.info('xpath is good')
        return result
    except Exception as e:
        print (e)
        return False

# use lxml to construct tree from html string
def construct_tree(htmlPage):
    try:
        node = html.fromstring(htmlPage)
        return node
    except Exception as e:
        print(e)
        return None

# get all nodes satisfying xpath inside one node, return empty list
# if no nodes satisfying or exception happens
def get_node_list(node, path):
    try:
        parse_logger.info('xpath is ' + path)
        node_list = node.xpath(path)
        return node_list
    except Exception as e:
        parse_logger.exception(e)
        return []

# get first node satisfying xpath insde one node, then retrieve
# this node's text property, return defaultValue if this node has
# no text property, or cannot find this node at all.
def text(node, path, default_value):
    try:
        if not check_xpath(path):
            return default_value
        node_list = node.xpath(path)
        if len(node_list) == 0:
            return default_value
        content = node_list[0].text
        # got node by xpth, but content is empty
        if content is None or not content.strip():
            return default_value
        # got value correctly
        return content
    except Exception as e:
        parse_logger.exception(e)
        # any exception happens, return defaultValue
        return default_value

# get first node satisfying xpath inside one node, then find all descdant nodes
# having text property, and concatenate their text with delimiter. Note each text
# is stripped before concatenated, and you can overwrite this if you do not want that.
def text_deep(node, path, default_value, delimiter):
    try:
        if not check_xpath(path):
            return default_value
        element_list = node.xpath(path)
        if len(element_list) == 0:
            return default_value
        element = element_list[0]
        children = element.xpath('.//text()')
        # print ('found text children:'+str(len(children)))
        content = ' '
        for child in children:
            try:
                # concatenate the text
                content = content + child.strip() + delimiter
            except:
                pass
        if content is None or not content.strip():
            return default_value
        return content
    except:
        return default_value

# get first node satisfying xpath inside one node, then get the node's attribute property
# return defaultValue if no node found, or no such attribute, or the value is stripped
# to emtpy string
def attribute(node, path, attribute_explicit, default_value):
    try:
        if not check_xpath(path):
            return default_value
        element_list = node.xpath(path)
        if len(element_list) == 0:
            return default_value
        element = element_list[0]
        attributeValue = element.get(attribute_explicit)
        if attributeValue is None or not attributeValue.strip():
            return default_value
        return attributeValue
    except Exception as e:
        parse_logger.exception(str(e))
        return default_value

# wrapper of attribute() function, e.g, './/img' and './/img/@data-src' are both allowed,
# but you need to specify attribute in function parameter when using formmer. If you use the latter,
# the attribute after split call (aka path_list[1]) will override the attribute function parameter.
def attribute_smart(node, path, attribute_explicit, default_value):
    try:
        if not check_xpath(path):
            return default_value
        if ('@' in path):
            path_list = path.split('/@')
            return attribute(node, path_list[0], path_list[1], default_value)
        return attribute(node, path, attribute_explicit, default_value)
    except Exception as e:
        parse_logger.exception(e)
        return default_value

# similar to text_deep,
def attribute_deep(node, path, attribute, default_value, delimiter):
    try:
        if not check_xpath(path):
            return default_value
        element_list = node.xpath(path)
        if len(element_list) == 0:
            return default_value
        element = element_list[0]
        # filter nodes with this attribute
        attri_path = Template('.//$attr').substitute(attr=attribute)
        children = element.xpath(attri_path)
        # print ('found text children:'+str(len(children)))
        content = ''
        for child in children:
            try:
                content = content + child + delimiter
            except:
                pass
        if content is None or not content.strip():
            return default_value
        content = content[:-len(delimiter)]
        return content.strip()
    except:
        return default_value

# get first node satisfying xpath insde given node, and return its inner html formatted
def inner_html(node, path, default_value):
    try:
        element_list = node.xpath(path)
        if len(element_list) == 0:
            return default_value
        element = element_list[0]
        return etree.tostring(element, pretty_print=True)
    except:
        return default_value

def price_number(price_text):
    try:
        price_pattern = re.compile(r"(\d{1,3}(\,\d{3})*|(\d+))(\.\d{1,2})?", re.IGNORECASE)
        # price_text = parse.text_deep(node, self.detail_config['price'], '', ' ')
        # self.logger.info('found price text:' + price_text)
        result = re.search(price_pattern, price_text)
        if result:
            price = result.group(0).replace(',', '')
        else:
            price = 0
        # self.logger.info('found price:' + price)
        return float(price)
    except Exception as e:
        print(str(e))
        return 0

def main():
    print("yes")
    htmlPage = ""
    # contruct a tree
    construct_tree(htmlPage)

if __name__ == "__main__":
    main()

